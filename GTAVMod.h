#pragma once

#include "inc/natives.h"
#include "keyboard.h"

#include <string>

class GTAVMod
{
private:
	const int k_ticksPerSecond = 1000;
	const DWORD k_keyToToggleStart = VK_F8;
	const int k_initialTimeToGameOver = 36;
	const int k_timeToGameOverStepPerWantedLevel = 6;

	Player m_player;
	Ped m_playerPed;

	bool m_isGamePlaying;

	std::string m_logMessage;

	DWORD m_startTick;
	DWORD m_gameOverTick;

	int m_kills;

	Ped m_currentlyTargettedPed;

	int m_previousWantedLevel;

	int m_bestKills;
	int m_bestTimeInSeconds;

	inline bool CheckForToggleIsGamePlaying();
	void ToggleIsGamePlaying();

	void StartGame();
	void UpdateGame();
	void EndGame();

	void SetupUI();
	void UpdateUI();

	void DisplayLogMessage();
	void UpdateLogMessage(std::string format, ...);

	void UpdateGameStats();
	inline int GetSecondsUntilGameOver();
	inline int GetTotalSecondsPlayed();

	inline int GetTicksUntilGameOver();
	inline int GetMaxTicksUntilGameOver();
	inline void RefreshTicksToGameOver();
	void CheckForGameOver();

	void UpdateCurrentlyTargettedPed();
	inline BOOL CheckHasPedBeenKilled(Ped ped);
	void CheckForKilledTargettedPed();
	void OnKilledTargettedPed();

	inline int GetPlayerMaxHealth();
	inline void SetPlayerCurrentHealth(int health);
	inline void ResetPlayerCurrentHealth();
	
	inline int GetPlayerWantedLevel();
	inline void ClearPlayerWantedLevel();
	void CheckForWantedLevelIncrease();
	void OnWantedLevelIncrease();

public:
	GTAVMod();
	~GTAVMod();

	void Start();
	void Update();
};


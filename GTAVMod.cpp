#include "GTAVMod.h"
#include <sstream>

GTAVMod::GTAVMod()
	: m_isGamePlaying(false)
	, m_bestKills(0)
	, m_bestTimeInSeconds(0)
{
}

GTAVMod::~GTAVMod()
{
}

void GTAVMod::Start()
{
	m_player = PLAYER::PLAYER_ID();
	
	m_playerPed = PLAYER::PLAYER_PED_ID();

	SetupUI();
}

void GTAVMod::Update()
{
	UpdateUI();

	if (CheckForToggleIsGamePlaying())
	{
		ToggleIsGamePlaying();
	}

	if (m_isGamePlaying)
	{
		UpdateGame();
	}
}

inline bool GTAVMod::CheckForToggleIsGamePlaying()
{
	return IsKeyJustUp(k_keyToToggleStart);
}

void GTAVMod::ToggleIsGamePlaying()
{
	if (m_isGamePlaying)
	{
		EndGame();
	}
	else
	{
		StartGame();
	}
}

void GTAVMod::StartGame()
{
	m_isGamePlaying = true;

	m_startTick = GetTickCount();

	m_kills = 0;

	RefreshTicksToGameOver();

	ClearPlayerWantedLevel();
}

void GTAVMod::UpdateGame()
{
	UpdateCurrentlyTargettedPed();

	CheckForKilledTargettedPed();

	CheckForWantedLevelIncrease();

	CheckForGameOver();
}

void GTAVMod::EndGame()
{
	m_isGamePlaying = false;

	ResetPlayerCurrentHealth();

	ClearPlayerWantedLevel();
}

void GTAVMod::SetupUI()
{
}

void GTAVMod::UpdateUI()
{
	UpdateGameStats();

	DisplayLogMessage();
}

void GTAVMod::DisplayLogMessage()
{
	UI::SET_TEXT_FONT(0);
	UI::SET_TEXT_SCALE(0.55, 0.55);
	UI::SET_TEXT_COLOUR(255, 255, 255, 255);
	UI::SET_TEXT_WRAP(0.0, 1.0);
	UI::SET_TEXT_CENTRE(1);
	UI::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
	UI::SET_TEXT_EDGE(1, 0, 0, 0, 205);

	UI::_SET_TEXT_ENTRY("STRING");
	UI::_ADD_TEXT_COMPONENT_STRING((char *)m_logMessage.c_str());

	UI::_DRAW_TEXT(0.5, 0.85);
}

void GTAVMod::UpdateLogMessage(std::string format, ...)
{
	char buffer[512];
	va_list args;
	va_start(args, format);
	vsnprintf_s(buffer, 511, format.c_str(), args);
	va_end(args);

	m_logMessage = buffer;
}

void GTAVMod::UpdateGameStats()
{
	if (m_isGamePlaying)
	{
		int secondsPlayed = GetTotalSecondsPlayed();

		if (secondsPlayed > m_bestTimeInSeconds)
		{
			m_bestTimeInSeconds = secondsPlayed;
		}

		if (m_kills > m_bestKills)
		{
			m_bestKills = m_kills;
		}

		std::stringstream ss;
		ss << "Seconds remaining: ";
		ss << GetSecondsUntilGameOver();
		ss << "\nTotal time: ";
		ss << GetTotalSecondsPlayed();
		ss << "\nKills: ";
		ss << m_kills;
		ss << "\n\nBest time: ";
		ss << m_bestTimeInSeconds;
		ss << "\nBest kills: ";
		ss << m_bestKills;
		
		UpdateLogMessage(ss.str());
	}
	else
	{
		UpdateLogMessage("Best time: %d\nBest kills: %d", m_bestTimeInSeconds, m_bestKills);
	}
}

inline int GTAVMod::GetSecondsUntilGameOver()
{
	return GetTicksUntilGameOver() / k_ticksPerSecond;;
}

inline int GTAVMod::GetTotalSecondsPlayed()
{
	return (GetTickCount() - m_startTick) / k_ticksPerSecond;
}

inline int GTAVMod::GetTicksUntilGameOver()
{
	return m_gameOverTick - GetTickCount();
}

inline int GTAVMod::GetMaxTicksUntilGameOver()
{
	return (k_initialTimeToGameOver - k_timeToGameOverStepPerWantedLevel * GetPlayerWantedLevel()) * k_ticksPerSecond;
}

inline void GTAVMod::RefreshTicksToGameOver()
{
	m_gameOverTick = GetTickCount() + GetMaxTicksUntilGameOver();
}

void GTAVMod::CheckForGameOver()
{
	if (GetTickCount() > m_gameOverTick || (PLAYER::GET_TIME_SINCE_LAST_DEATH() < 1000 && PLAYER::GET_TIME_SINCE_LAST_DEATH() >= 0) || (PLAYER::GET_TIME_SINCE_LAST_ARREST() < 1000 && PLAYER::GET_TIME_SINCE_LAST_ARREST() >= 0))
	{
		EndGame();
	}
}

void GTAVMod::UpdateCurrentlyTargettedPed()
{
	Entity targettedEntity;
	
	if (PLAYER::IS_PLAYER_TARGETTING_ANYTHING(m_player))
	{
		PLAYER::GET_PLAYER_TARGET_ENTITY(m_player, &targettedEntity);
	}
	else if (PLAYER::IS_PLAYER_FREE_AIMING(m_player))
	{
		PLAYER::GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(m_player, &targettedEntity);
	}

	if (ENTITY::IS_ENTITY_A_PED(targettedEntity))
	{
		Ped targettedPed = ENTITY::GET_PED_INDEX_FROM_ENTITY_INDEX(targettedEntity);

		if (!CheckHasPedBeenKilled(targettedPed))
		{
			m_currentlyTargettedPed = targettedPed;
		}
	}
}

inline BOOL GTAVMod::CheckHasPedBeenKilled(Ped ped)
{
	return PED::IS_PED_FATALLY_INJURED(ped);
}

void GTAVMod::CheckForKilledTargettedPed()
{
	if (m_currentlyTargettedPed != -1 && CheckHasPedBeenKilled(m_currentlyTargettedPed))
	{
		OnKilledTargettedPed();
	}
}

void GTAVMod::OnKilledTargettedPed()
{
	m_currentlyTargettedPed = -1;

	m_kills++;

	ResetPlayerCurrentHealth();

	RefreshTicksToGameOver();
}

inline int GTAVMod::GetPlayerMaxHealth()
{
	return PED::GET_PED_MAX_HEALTH(m_playerPed) - 100;
}

inline void GTAVMod::SetPlayerCurrentHealth(int health)
{
	ENTITY::SET_ENTITY_HEALTH(m_playerPed, health + 100);
}

inline void GTAVMod::ResetPlayerCurrentHealth()
{
	SetPlayerCurrentHealth(GetPlayerMaxHealth());
}

inline int GTAVMod::GetPlayerWantedLevel()
{
	return PLAYER::GET_PLAYER_WANTED_LEVEL(m_player);
}

inline void GTAVMod::ClearPlayerWantedLevel()
{
	PLAYER::CLEAR_PLAYER_WANTED_LEVEL(m_player);

	m_previousWantedLevel = 0;
}

void GTAVMod::CheckForWantedLevelIncrease()
{
	if (GetPlayerWantedLevel() > m_previousWantedLevel)
	{
		OnWantedLevelIncrease();
	}
}

void GTAVMod::OnWantedLevelIncrease()
{
	int wantedLevel = GetPlayerWantedLevel();

	m_previousWantedLevel = wantedLevel;

	RefreshTicksToGameOver();
}

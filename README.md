A small mod for GTA V that I made over the course of a weekend that challenges players to keep up an increasingly rapid kill-streak.

A video of the mod in action can be found at [https://youtu.be/kXfi_H_EMeA](Link URL).

The rules are as follows:

1. A timer is constantly ticking down, if it hits zero, you lose.

2. If you are killed or arrested, you lose.

3. Every time you gun down a pedestrian, the timer and your health reset.

4. Every time your wanted level rises, the value that the timer rests to decreases.


To install:

1. Download Script Hook V from [http://dev-c.com/gtav/scripthookv/](Link URL) and extract the .zip file.

2. Copy the files "ScriptHook.v" and "dinput8.dll" into the installation directory for Grand Theft Auto V.

3. Clone this Git repo and build the solution contained inside.

4. Go to the folder containing the solution, then to ./bin/Release/ and copy the file GTAVMod.asi to the installation directory for GTA V.

5. Launch GTAV.

NOTE: Be sure to remove all of the added files from your GTA V installation folder before attempting to access GTA Online.

When in game just press F8 to start/stop the kill-streak challenge.

Thanks go to Rockstar Games, for creating GTA V, and also to Alexander Blade for creating ScriptHookV, the library utilised in order to create this mod. Some of the code contained within the repository is his work. The code that I wrote myself is almost entirely contained within the GTAVMod.h and GTAVMod.cpp files.

To see more software I've produced, feel free to visit my portfolio at [www.joerobbins.co.uk](Link URL).